﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace MusicPlayer
{
    class Utilites
    {

        /// <summary>
        /// テンポラリフォルダ内の削除
        /// </summary>
        public static void CleanTempFolder()
        {
            //テンポラリフォルダ内を全削除
            IEnumerable<string> listFiles = Directory.EnumerateFiles(GetTmpFolder(), "*", SearchOption.TopDirectoryOnly);
            foreach (string file in listFiles)
            {
                try
                {
                    File.Delete(file);
                }
                catch (Exception)
                {
                }
            }
        }


        /// <summary>
        /// tmpフォルダの作成
        /// </summary>
        public static void CreateTmpFolder()
        {
            try
            {
                Directory.CreateDirectory(GetTmpFolder());
            }
            catch (Exception)
            {
            }
        }


        /// <summary>
        /// テンポラリファイル名の作成
        /// </summary>
        public static string GetTmpFile()
        {
            return GetTmpFolder() + DateTime.Now.Ticks;
        }

        /// <summary>
        /// テンポラリフォルダを返す
        /// </summary>
        public static string GetTmpFolder()
        {
            return GetExeFolder() + @"tmp\";
        }


        /// <summary>
        /// 実行ファイルのあるフォルダパスを返す
        /// </summary>
        public static string GetExeFolder()
        {
            FileInfo cFileInfo2 = new FileInfo(Application.ExecutablePath);     //実行ファイルのあるフォルダを取得するために実行ファイルの詳細を取得
            return cFileInfo2.DirectoryName + @"\";
        }


        /// <summary>
        /// ZIPファイルからの1ファイル解凍
        /// </summary>
        /// <param name="strZipFile">解凍したいzipファイルへのパス</param>
        /// <param name="nEntryIndex">zip書庫内のエントリーインデックス</param>
        /// <param name="strOutFileName">解凍先のフォルダ名＋ファイル名（拡張子なしで指定）</param>
        /// <param name="strOutFilePath">解凍先ファイルのフルパス。↑に拡張子が付加される</param>
        /// <returns></returns>
        public static bool ExtractFileFromZip(string strZipFile, int nEntryIndex, string strOutFileName, out string strOutFilePath)
        {
            strOutFilePath = "";
            if (strZipFile == "" || strOutFileName == "" || nEntryIndex < 0)
                return false;
            try
            {
                bool ret = false;

                using (ZipArchive archive = ZipFile.OpenRead(strZipFile))
                {
                    if (nEntryIndex < archive.Entries.Count)
                    {
                        string strExtension = Path.GetExtension(archive.Entries[nEntryIndex].FullName);
                        archive.Entries[nEntryIndex].ExtractToFile(strOutFileName + strExtension, true);
                        strOutFilePath = strOutFileName + strExtension;
                        ret = true;
                    }
                }
                return ret;
            }
            catch (Exception)
            {
            }
            return false;
        }


    }
}
