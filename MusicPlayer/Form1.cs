﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace MusicPlayer
{
    public partial class Form1 : Form
    {
        WindowsMediaPlayer mediaPlayer = new WindowsMediaPlayer();

        Timer timer = new Timer();

        class AudioItem
        {
            public string strFile = "";
            public int entryIndexNumber = -1;

            public AudioItem(string strFile, int entryIndexNumber = -1)
            {
                this.strFile = strFile;
                this.entryIndexNumber = entryIndexNumber;
            }
        }

        List<AudioItem> listFiles; //音楽ファイルの一覧
        int currentIndexNumber = -1;    //↑へのインデックス

        List<int> listPlayHistory = new List<int>(); //過去の再生曲
        List<int> listPlayNext = new List<int>();    //未来の再生曲、なければランダム

        bool bStopButtonPushed = false; //「停止」ボタンを押したらtrue

        

        public Form1()
        {
            InitializeComponent();

            Utilites.CreateTmpFolder();

            listFiles = EnumFiles(@"C:\Users\Public\Music"); //再生したいmp3保存フォルダを指定


            //フォームが表示されたときの処理
            Shown += delegate
            {
                //シークバーにフォーカスがあると、再生位置が表示されないので
                //ここで再生ボタンにフォーカスを与える
                buttonPlay.Focus();
            };

            //終了時処理
            FormClosed += delegate
            {
                Utilites.CleanTempFolder();          //tmpフォルダ内の削除
            };

            //再生位置のシーク
            trackBarSeek.ValueChanged += delegate
            {
                if (trackBarSeek.Focused)
                {
                    mediaPlayer.controls.currentPosition = (double)trackBarSeek.Value / 100;
                }
            };

            //音量の調節
            mediaPlayer.settings.volume = 20;
            trackBarVolume.Maximum = 100; //音量は0～100
            trackBarVolume.Value = mediaPlayer.settings.volume;
            trackBarVolume.ValueChanged += delegate
            {
                mediaPlayer.settings.volume = (int)trackBarVolume.Value;
            };

            timer.Interval = 100; //100msec毎に処理を実行する
            timer.Tick += delegate
            {
                if (mediaPlayer.playState == WMPPlayState.wmppsPlaying || mediaPlayer.playState == WMPPlayState.wmppsPaused)
                {
                    trackBarSeek.Maximum = (int)(mediaPlayer.controls.currentItem.duration * 100);
                    //シークバーにフォーカスがないときだけ再生位置を表示する
                    if (trackBarSeek.Focused == false)
                    {
                        try
                        {
                            //シークバーに再生位置を表示
                            //曲変更タイミングによっては例外が出る可能性がある
                            trackBarSeek.Value = (int)(mediaPlayer.controls.currentPosition * 100);
                        }
                        catch (Exception)
                        {
                        }
                    }

                    //再生時間の表示
                    labelTime.Text = mediaPlayer.controls.currentPositionString;

                }

                //止まっていたら再び再生。ただし、停止ボタンが押されている場合は無視
                if (mediaPlayer.playState == WMPPlayState.wmppsStopped)
                {
                    if (bStopButtonPushed == false)
                        Play();
                }
            };
            timer.Start();


            //WMPのコントロール部分
            mediaPlayer.PlayStateChange += delegate (int nNewState)
            {
                WMPPlayState state = (WMPPlayState)nNewState;

                switch (state)
                {
                    case WMPPlayState.wmppsStopped:
                        buttonPlay.Text = "＞";
                        trackBarSeek.Value = 0;
                        labelTime.Text = "00:00";
                        break;

                    case WMPPlayState.wmppsPaused:
                        timer.Stop();
                        buttonPlay.Text = "＞";
                        break;

                    case WMPPlayState.wmppsPlaying:
                        bStopButtonPushed = false;
                        buttonPlay.Text = "| |";
                        timer.Start();
                        break;

                    case WMPPlayState.wmppsTransitioning:
                        break;
                }
            };
            Play();
        }



        ///<summary>
        ///曲を再生する
        ///</summary>
        void Play(bool bPlayNext = true)
        {
            if (listFiles.Count == 0)
                return;

            if(bPlayNext || listPlayHistory.Count == 0)
            {
                //次の曲を再生する
                if(currentIndexNumber >= 0)
                {
                    listPlayHistory.Add(currentIndexNumber);
                    if (listPlayHistory.Count > 100)
                        listPlayHistory.RemoveAt(0);
                }
                currentIndexNumber = GetNextIndex();
            }
            else
            {
                //前の曲を再生する
                if (currentIndexNumber >= 0)
                    listPlayNext.Insert(0, currentIndexNumber);                  //今の曲を次の曲として保存
                currentIndexNumber = listPlayHistory[listPlayHistory.Count - 1]; //一番最新の履歴曲を今の曲とする
                listPlayHistory.RemoveAt(listPlayHistory.Count - 1);             //一番最新の履歴曲を削除
            }
            
            Utilites.CleanTempFolder();          //tmpフォルダ内の削除

            
            string strFile = listFiles[currentIndexNumber].strFile;
            if (listFiles[currentIndexNumber].entryIndexNumber >= 0)
            {
                //zip書庫ならtmpフォルダに解凍する
                bool ret = Utilites.ExtractFileFromZip(strFile, listFiles[currentIndexNumber].entryIndexNumber, Utilites.GetTmpFile(), out strFile);
                if (ret == false)
                    return;
            }

            
            mediaPlayer.URL = strFile;         //再生曲をセット
            mediaPlayer.controls.play();

            
            
            
            //ファイル名からタイトルラベルへ表示。　WMP純正の音楽ファイルタグ読み込み方法
            
            //string titleName = Path.GetFileName(mediaPlayer.URL);
            //labelTitle.Text = titleName;
            //当初は上記の「GetFileName」の仕組みを使用していたが、廃止

            mediaPlayer.currentMedia.setItemInfo(string.Empty, string.Empty);   //この記述がないと正常に動作しないとのこと


            string titleName = mediaPlayer.currentMedia.getItemInfo("Title");   //labelTitleにタイトル名を表示
            labelTitle.Text = titleName;

            string artistName = mediaPlayer.currentMedia.getItemInfo("Artist"); //labelArtistにアーティスト名を表示
            labelArtist.Text = artistName;

            string albumName = mediaPlayer.currentMedia.getItemInfo("Album");   //labelAlbumにアルバム名を表示
            labelAlbum.Text = albumName;

        }


        ///<summary>
        ///次の再生曲インデックスを決める
        ///</summary>
        int GetNextIndex()
        {
            if (listFiles.Count == 0)
                return -1;
            int nextIndex;
            Random rnd = new Random(Environment.TickCount);

            while(true)
            {
                if(listPlayNext.Count == 0)
                {
                    //未来の曲のリストが空ならランダムで曲を決定
                    nextIndex = rnd.Next(0, listFiles.Count);
                }
                else
                {
                    //未来の曲リストから次の曲を決定
                    nextIndex = listPlayNext[0];
                    listPlayNext.RemoveAt(0);
                }

                //同じ曲は連続で再生しない
                if (nextIndex == currentIndexNumber)
                    continue;

                break;
            }

            return nextIndex;
        }


        ///<summary>
        ///フォルダ内のファイルを一覧して返す
        ///</summary>
        List<AudioItem> EnumFiles(string strFolder)
        {
            List<AudioItem> ret = new List<AudioItem>();

            //指定フォルダ以下の全子フォルダから全ファイルを抜き出す
            IEnumerable<string> listFiles = Directory.EnumerateFiles(strFolder, "*.*", SearchOption.AllDirectories);

            foreach (string strFile in listFiles)
			{
				//見つかったファイルの拡張子を取り出し
				string stringExtention = Path.GetExtension(strFile).ToLower();
				if (stringExtention == "")
					continue;

			
				//mp3、wavファイルならそのまま追加
				if (stringExtention == ".mp3" || stringExtention == ".wav")
				{
					ret.Add(new AudioItem(strFile));
					continue;
				}

				
				//ZIPファイルなら書庫に含まれるmp3、wavファイルのエントリーを追加
				if (stringExtention == ".zip")
				{
					try
					{
						using (ZipArchive archive = ZipFile.OpenRead(strFile))
						{
							for (int i = 0; i < archive.Entries.Count; i++)
							{
								string strExtensionZip = Path.GetExtension(archive.Entries[i].FullName).ToLower();
								if (strExtensionZip == ".mp3" || strExtensionZip == ".wav")
								{
									ret.Add(new AudioItem(strFile, i));
								}
							}
						}
					}
					catch (Exception)
					{
					}
				}
			}

			return ret;
		}



        private void buttonPlay_Click(object sender, EventArgs e)
        {
            if (mediaPlayer.playState == WMPPlayState.wmppsPlaying)
                mediaPlayer.controls.pause();
            else if (currentIndexNumber >= 0)
                mediaPlayer.controls.play();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if(mediaPlayer.playState == WMPPlayState.wmppsPlaying || mediaPlayer.playState == WMPPlayState.wmppsPaused)
            {
                bStopButtonPushed = true;
                mediaPlayer.controls.stop();
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            Play();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            Play(false);
        }
    }
}
